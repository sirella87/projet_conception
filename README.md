# Projet de L3 : "Cryptanalyse différentielle sur un algorithme de DES à 4 tours"
##### Par Sirella Lefort-Bédoni et Vincent Fély (Encadrant P. Gaborit)
###### Faculté des Sciences et Technique de l'Université de Limoges (2014-2015)

## Objectifs
L'objectif de ce projet est de reproduire une version simplifiée de l'algorithme du DES (quatre tours au lieu de seize) puis de réaliser une attaque de cryptanalyse différentielle sur notre algorithme.
Le projet se compose de trois programmes remplissant les fonctions suivantes :

* crypt.e : Chiffrement d'un message
* decrypt.e : Déchiffrement d'un message 
* cryptanalyse_dif.e : Attaque de cryptanalyse différentielle

## Réalisation
Bien qu'à l'état de développement, l'ensemble du projet est d'ores et déjà téléchargeable et compilable.
```sh
$ git clone https://bitbucket.org/sirella87/projet_conception.git
$ cd projet_conception
$ make install
```
### Partie 1 : Reproduction de l'algorithme du DES à quatre tours et chiffrement d'un message
Dans cette partie on reproduira dans un premier temps l'algorithme du DES à quatre tours dont le principe est décrit ci-dessous. Ensuite on réalisera un algorithme de chiffrement d'un message utilisant notre version réduite du DES.

#### Principe de l'algorithme du DES
##### CLEF

Pour chaque tour (4 au total) faire :

* Reduction de la clef  de 64 à 56 bits avec la matriceReduc et mélange de cette clef
* Division de la clef en partie droite et gauche, chacune de 28 bits
* Rotation des deux parties de la clef de un ou deux bits vers la gauche suivant le numéro de tour où l'on se trouve
* Concaténation des deux parties pour former une clef de 56 bits
* Deuxième réduction de 56 vers 48 bits et mélange de la clef grace à la matriceReduction2

##### TEXTE

* Permutation initiale du texte grace à la matriceInitiale
Pour chaque tour faire :
* Division du texte en deux partie droite et gauche de 32 bits chacunes
* La partie gauche reçoit la partie droite du tour précédent
* La partie droite est elle étendue sur 48 bits et mélangée gràace à la matriceExt
* On effectue ensuite un XOR entre cette partie droite étendue et la clef correspondante au tour où l'on se trouve
* Ce que l'on obtient est ensuite divisée en 8 partie qui "passe" chacune dans une S-Box...........FLOU
* La partie droite est alors de 28 bits et recoit : partie gauche XOR avec ce que l'on a obtenu à l'étape précédente
* On inverse ensuite la partie droite et la partie gauche et on concatene pour avoir une chaine de 64 bits comme initialement
* On effectue en dernier une permutation finale grace la matriceFinale
* Le texte est alors crypté

#### Partie terminée (02/03/2015)

Le premier argument à renseigner est la clé de chiffrement. Le second est le texte à crypter.
```sh
$ ./crypt.e "Clé de chiffrement" "Texte à crypter"
```
On obtient ainsi un texte chiffré sous forme hexadécimale.

### Partie 2 : Décryptage d'un message
Dans cette partie on décryptera un message préalablement crypté en utilisant notre version simplifié du DES. Pour cela on réutilisera la fonction **des()** utilisée dans crypt.c dans laquelle on inversera les clés.

#### Partie terminée (15/03/2015)

Le premier argument à renseigner est la clé de chiffrement. Le second est le code hexadécimal à décrypter.
```sh
$ ./decrypt.e "Clé de chiffrement" "Texte à décrypter"
```
On obtient ainsi un texte en clair.

### Partie 3 : Attaque de cryptanalyse différentielle

La cryptanalyse différentielle consiste en l'étude des différences entre les données en entrée d’un algorithme de cryptage et celles en sortie. Elle suppose qu’on dispose du programme réalisant le chiffrement et qu’on est donc capable de lui transmettre en entrée les données de notre choix. Si l’algorithme est vulnérable à ce type d’attaque on est en mesure de récupérer la clé ayant servi au chiffrement des données.

Dans cette partie on mettra en application cette attaque contre notre algorithme de DES à quatre tours.

#### TO DO :
* 

## Documentation et outils
* [The DES Algorithm Illustrated] par J. Orlin Grabbe
* [Online encrypt tool]
* [Open Security Research]
* [Table des caractères spéciaux en LaTeX]
* [Les Mathématiques en LaTeX]

[The DES Algorithm Illustrated]:http://page.math.tu-berlin.de/~kant/teaching/hess/krypto-ws2006/des.htm
[Online encrypt tool]:http://www.tools4noobs.com/online_tools/encrypt/
[Open Security Research]:http://calc.opensecurityresearch.com/
[Table des caractères spéciaux en LaTeX]:http://www.commentcamarche.net/contents/620-latex-table-de-caracteres
[Les Mathématiques en LaTeX]:https://fr.wikibooks.org/wiki/LaTeX/Mathématiques
