\select@language {french}
\contentsline {chapter}{Introduction}{2}
\contentsline {chapter}{\numberline {1}Pr\IeC {\'e}liminaires}{3}
\contentsline {section}{\numberline {1.1}D\IeC {\'e}finitions et histoire}{3}
\contentsline {subsection}{\numberline {1.1.1}D\IeC {\'e}finitions}{3}
\contentsline {subsection}{\numberline {1.1.2}Les algorithmes de chiffrement}{3}
\contentsline {subsection}{\numberline {1.1.3}La cryptanalyse}{4}
\contentsline {section}{\numberline {1.2}Objectifs et contraintes du projet}{4}
\contentsline {subsection}{\numberline {1.2.1}D\IeC {\'e}finition des objectifs}{4}
\contentsline {subsection}{\numberline {1.2.2}Choix des outils et gestion du projet}{5}
\contentsline {chapter}{\numberline {2}Reproduction de l'algorithme du DES}{6}
\contentsline {section}{\numberline {2.1}Th\IeC {\'e}orie}{6}
\contentsline {section}{\numberline {2.2}Pratique}{7}
\contentsline {subsection}{\numberline {2.2.1}G\IeC {\'e}n\IeC {\'e}ration des n cl\IeC {\'e}s}{7}
\contentsline {subsection}{\numberline {2.2.2}Traitement du message sur n tours}{7}
\contentsline {chapter}{\numberline {3}Cryptanalyse de notre algorithme}{9}
\contentsline {section}{\numberline {3.1}Choix de la cryptanalyse diff\IeC {\'e}rentielle}{9}
\contentsline {section}{\numberline {3.2}Analyses pr\IeC {\'e}alables}{9}
\contentsline {subsection}{\numberline {3.2.1}Analyse des boites-S}{10}
\contentsline {subsection}{\numberline {3.2.2}Recherche d'un diff\IeC {\'e}rentiel}{11}
\contentsline {section}{\numberline {3.3}Cryptanalyse}{11}
\contentsline {subsection}{\numberline {3.3.1}G\IeC {\'e}n\IeC {\'e}ration des couples clairs/chiffr\IeC {\'e}s}{12}
\contentsline {subsection}{\numberline {3.3.2}R\IeC {\'e}cup\IeC {\'e}ration des bits de la cl\IeC {\'e}}{12}
\contentsline {chapter}{Conclusion}{14}
\contentsline {chapter}{Bibliographie}{15}
\contentsline {chapter}{Table des illustrations}{16}
