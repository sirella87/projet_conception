\documentclass[a4paper,10pt]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[francais]{babel}
\usepackage{listings}
\usepackage{graphicx}

\graphicspath{{images/}}

\title{Cryptanalyse différentielle sur un algorithme de DES à 4 tours}
\author{Sirella Lefort-Bédoni \& Vincent Fély
        \and Encadrant : Philippe Gaborit}
\date{\today}

\begin{document}

%%% PAGE DE TITRE %%%
\maketitle

%%% PAGE DE REMERCIEMENTS %%%
\chapter*{Remerciements}
\thispagestyle{empty}
	Nous tenons à remercier Philippe Gaborit de nous avoir proposé ce projet et d'avoir lors de sa réalisation accepté de partager avec nous son expérience dans le domaine.\\
	Nous remercions également nos proches, famille et amis, pour la patience dont ils ont fait preuve lors de la relecture de ce dossier.

%%% SOMMAIRE %%%
\tableofcontents
\setcounter{page}{1}

%%% INTRODUCTION %%%
\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}
	Nous sommes en 2015, ma télévision connaît mes programmes favoris et mon réfrigérateur est mieux renseigné que mon diététicien sur mes habitudes alimentaires. De toute façon à quoi bon avoir un diététicien depuis que ma montre s'est mise de mèche avec ma balance connectée pour m'indiquer chaque matin que mon Indice de Masse Corporelle est en hausse de +0,1\% depuis les 15 derniers jours et que le stade le plus proche est à moins de 200 mètres.\newline

	Le portrait dressé est volontairement caricatural mais il n'en est pas moins réaliste quant à l'intrusion de la technologie connectée dans nos quotidiens. C'est autant de données nous concernant qui sont créées chaque jours et stockées à des milliers de kilomètres. Cela pose le problème de la protection de nos vies privées.\newline
	Il me semble que cela soulève deux questions: Celle de la collecte de données et celle de la sécurité mise en oeuvre pour les stocker. C'est sur cette dernière que portera notre analyse.\newline
	
	La sécurité des informations est primordiale et assurera demain la garantie de la vie privée des utilisateurs. Ces dernières années les algorithmes de chiffrement n'ont cessé de se perfectionner et ne cesseront de le faire dans les années à venir. La reproduction du DES, l'un des algorithmes de chiffrement parmi les plus répandus de ces dernières années, nous permettra dans un premier temps de découvrir les différentes méthodes cryptographiques utilisées. Certaines de ces méthodes présentent cependant des vulnérabilités qu'il nous faudra exploiter dans un second temps en réalisant une cryptanalyse différentielle sur notre algorithme du DES dans sa version limitée à quatre tours.

%%% CHAPITRE 1 %%%
\chapter{Préliminaires}
	\section{Définitions et histoire}

		\subsection{Définitions}
			\begin{description}
				\item [Cryptologie :] La cryptologie regroupe la cryptographie qui consiste à chiffrer des messages et la cryptanalyse.
				\item [DES :]  Le DES (Data Encryption Standard) est un algorithme de chiffrement par bloc à clé secrète. 
				\item [Cryptanalyse :] La cryptanalyse est l'ensemble des méthodes permettant de déchiffrer un message codé sans connaître la clé de chiffrement.
			\end{description}
		

		\subsection{Les algorithmes de chiffrement}
			La cryptographie existe depuis l'antiquité et s'est fait connaître avec le chiffre de César. En effet ce dernier utilisait la cryptographie pour chiffrer ses communications secrètes. Le chiffre de César est un chiffrement par décalage qui consiste à décaler les lettres du texte clair d'un nombre fixe vers un côté donné de l'alphabet. 

			\begin{figure}[h]
				\centering
				\includegraphics[scale=0.5]{cesar.jpg}
				\caption{Chiffre de César}
			\end{figure}

			C'est une science nouvelle car ce n'est un thème de recherche scientifique universitaire que depuis les années 1970. Cette science est liée à beaucoup d'autres, comme la théorie des nombres, l'algèbre, la théorie de la complexité, la théorie de l'information, ou encore les codes correcteurs.\\
Il existe deux chiffrements: le chiffrement à clé secrète, comme par exemple le DES ou l'AES, et le chiffrement à clé publique, comme par exemple le RSA.\\

Les algorithmes à clé secrète (ou algorithmes symétriques) possèdent la même clé pour chiffrer et pour déchiffrer le message. Il faut alors que les deux protagonistes s'échangent cette clé pour pouvoir communiquer. Il existe pour ces algorithmes des attaques exhaustives consistant à tester toutes les clés possibles. La difficulté augmente avec le nombre de bits de la clé. Ces attaques deviennent donc inutilisables à partir d'une clé faisant 64 bits car il faudrait alors $2^{63}$ essais pour retrouver la clé. On peut comparer le fonctionnement des algorithmes à clé secrète à un coffre fort : Tous les protagonistes doivent avoir la même clé pour ouvrir ce coffre. \\

Les algorithmes à clé publique (ou asymétrique) fonctionnent avec une clé publique mise à disposition de tous et une clé privée que chaque protagoniste garde secrète. Ces algorithmes peuvent être comparés à une boîte aux lettres dans laquelle la clé publique est l'adresse et la clé privée est la clef du propriétaire de la boîte. Tout le monde connaît l'adresse de la boîte aux lettres et peut donc y insérer des messages, mais seul le propriétaire peut l'ouvrir et récupérer son contenu.

		\subsection{La cryptanalyse}
			La cryptanalyse est très ancienne. Au 8ème siècle un linguiste du nom d'Al Farahidi étudiait déjà des méthodes d'attaque à textes clairs connus ou des méthodes à mots probables. Ce n'est qu'un siècle plus tard qu'Al-Kindi, un philosophe musulman, rédigea le plus ancien manuscrit sur la cryptologie en exposant l'attaque par analyse fréquentielle.\\
Durant la seconde guerre mondiale, les allemands utilisèrent la cryptographie avec leur machine "Enigma" pour chiffrer leurs messages radios. Alan Turing se servit de la cryptanalyse pour décrypter leurs messages et écourter la guerre. La cryptanalyse nécessite de grandes connaissances mathématiques ainsi que de grandes capacités de calcul. Les méthodes d'attaque les plus connues sont : 
			\begin{itemize}
			\item{L'analyse fréquentielle qui se base sur les fréquences d'apparition des lettres dans les mots de la langue du texte chiffré.}
			\item{L'attaque par force brute qui teste toutes les clés possibles.}
			\item{La cryptanalyse linéaire qui consiste à faire des approximations linéaires de l’algorithme de chiffrement.}
			\item{La cryptanalyse différentielle qui est l'étude des différences entre les données en entrée d’un algorithme de cryptage et celles en sortie.}
		\end{itemize}


	\section{Objectifs et contraintes du projet}
		\subsection{Définition des objectifs}
			Dans un premier temps notre objectif est de reproduire l'algorithme du DES. Il s'agit créer un programme accessible au plus grand nombre qui permette de chiffrer et déchiffrer un message. On veillera donc à créer une méthode facile pour compiler et exécuter notre programme. Son fonctionnement doit être simple et au besoin documenté pour l'utilisateur.
			
			Notre second objectif est de mettre en oeuvre une attaque de cryptanalyse sur notre algorithme dans sa version limitée à 4 tours.
			
		\subsection{Choix des outils et gestion du projet}
			Ce projet ne nécessitant pas après analyse des objectifs l'utilisation d'objets, nous avons fait le choix de le programmer en langage C.\\
			Pour la rédaction du rapport et la production des diapositives nous avons choisi de nous initier au langage \LaTeX.\\
			Pour travailler simultanément sur notre projet nous avons créé un dépôt Git sur bitbucket.org. En plus de proposer un système de gestion des versions et d'accès simultané sur nos fichiers, Git nous permet de tenir à jour un fichier \texttt{Readme.md} en syntaxe Markdown par le biais duquel nous tenons à jour l'avancée de notre projet. Il y est détaillé nos objectifs, les étapes qu'il nous reste à effectuer pour les atteindre, ainsi que nos sources de documentation. Ce dépôt est public et donc consultable depuis \texttt{https://bitbucket.org/sirella87/projet\_conception}. \\
			Enfin pour faciliter sa compilation par des personnes extérieures nous avons créé un Makefile.\\
			
			L'ensemble du projet est donc téléchargeable et compilable en quelques commandes :
\lstset{
	language=sh,
	basicstyle=\footnotesize
}

\begin{lstlisting}
$ git clone https://bitbucket.org/sirella87/projet_conception.git
$ cd projet_conception
$ make install
\end{lstlisting}
			
			Dans un premier temps nous avons fait des recherches sur le DES qui nous ont permises de nous faire une idée du fonctionnement global de l'algorithme.\\ Dans la première partie nous nous sommes divisés les tâches de façon à avancer en parallèle sur le projet : L'un dans le coeur du programme reproduisant la théorie de l'algorithme, et l'autre dans tout ce qui est nécessaire à son implémentation dans le programme final.\\
Lors de partie sur la cryptanalyse en revanche nous avons travaillé de pair de façon à confronter nos points de vue à chaque étape de sa réalisation.

%%% CHAPITRE 2 %%%
\chapter{Reproduction de l'algorithme du DES}
	\section{Théorie}
		Le DES est créé en 1976 par la NSA. C'est en réalité un algorithme de chiffrement du nom de "Lucifer" développé par IBM puis modifié par la NSA pour être plus résistant aux attaques.
		L'algorithme du DES chiffre un message de 64 bits en un autre message de 64 bits. Il est constitué de 16 étapes utilisant 16 sous-clés de 56 bits. Une fois ces 16 sous-clés générées on peut le diviser en 3 parties :
		\begin{itemize}
			\item{Permutation initiale et fixe d'un bloc}
			\item{16 itérations de transformations. Chaque itération dépend d'une clé partielle de 48 bits générée à partir de la clé initiale}
			\item{Permutation finale qui correspond à la fonction inverse de la permutation initiale}
		\end{itemize}

		\begin{figure}[h]
			\centering
			\includegraphics[scale=0.4]{DES16tours.png}
			\caption{Schéma du DES}
			\label{des16tours}
		\end{figure}


	\section{Pratique}
%Nous allons détailler le fonctionnement :
%		L'étape 1 consiste à générer les 16 clés à partir de la clé initiale de 64 bits. Pour cela nous commençons par effectuer un mélange de bits et une réduction de la clé initiale grâce à une matrice de permutation. La clé est alors réduite à 56 bits. On divise ensuite cette clé intermédiaire en partie gauche et droite. Puis on effectue une rotation des bits de chacun des côtés d'un nombre de tours fixé par une matrice. Pour finir on concatène les parties gauches et droites de la clé, puis on la repasse dans une matrice de permutation/réduction de bits. Nous obtenons ainsi les 16 clés de 48 bits pour chaque tour.
%		L'étape 2 consiste à effectuer un permutation initiale et à diviser le texte clair en deux parties droite et gauche. Puis effectuer des modifications sur la partie droite, échanger la partie droite et la partie gauche et recommencer un tour.

		\subsection{Génération des n clés}
		Notre algorithme du DES peut exécuter un nombre variable de tours. Nous pouvons ainsi le lancer à 16 tours pour nous assurer de son bon fonctionnement ou le lancer à 4 tours pour effectuer la cryptanalyse. La fonction de génération des clés prend en paramètre une variable qui indique si on crypte ou on décrypte.\\
		
		Nous commençons par générer les n clés correspondant aux n tours en effectuant la première réduction de la clé. Puis nous divisons la clé en partie gauche et partie droite et effectuons les rotations de bits en fonction de la matrice de rotation \texttt{matriceRotation[]} et du tour en cours de traitement. Nous concaténons la partie droite et gauche puis effectuons la deuxième réduction. Nous rangeons nos clés de tours dans un tableau où la ligne correspondra au tour du DES où la clé devra servir.\\
Dans le cas d'un cryptage les clés sont insérées dans le tableau dans l'ordre dans lequel elles sont générées. Lors du décryptage elles sont rangées dans le sens inverse.
		
		\subsection{Traitement du message sur n tours}
		Nous traitons ensuite le message à chiffrer en effectuant la permutation initiale. Puis nous divisons le texte en partie gauche (L) et partie droite (R).\\ Commencent alors les n tours du DES. Pour chaque tour nous étendons la partie droite, puis nous effectuons un XOR entre la clé du tour et la partie droite. Notre partie de droite modifiée passe ensuite dans des boîtes de substitution, "boites-S" ou "Sbox", qui réduisent la partie droite et mélangent les bits. S'effectue ensuite une permutation. La partie droite du tour suivant reçoit le XOR entre les parties gauches et droites du tour actuel et la partie gauche du tour suivant reçoit la partie droite non modifiée du tour actuel. À la fin des 4 tours nous concaténons les parties gauches et droites du tour 4 puis effectuons une permutation finale. Le texte est alors crypté.\\

		\begin{figure}[h]
			\centering
			\includegraphics[scale=0.4]{DES4tours.png}
			\caption{Schéma du DES à 4 tours}
			\label{des4tours}
		\end{figure}

		Le DES ne traite que des messages et des clés de 64 bits. Il faut donc préalablement découper nos textes clairs en blocs de 64 bits en les comblant si nécessaire de zéros. Pour la clé nous avons choisi de limiter son nombre de caractère à 8 de sorte à ne jamais dépasser les 64 bits.

%%% CHAPITRE 3 %%%
\chapter{Cryptanalyse de notre algorithme}
	\section{Choix de la cryptanalyse différentielle}
		Lors de nos recherches préliminaires nous avons choisi de nous documenter sur les deux cryptanalyses. Nous avons ensuite fait le choix de la cryptanalyse différentielle.\\
		
		La cryptanalyse différentielle consiste en l'étude des différences entre les données en entrée d’un algorithme de cryptage et celles en sortie. Elle suppose qu’on dispose du programme réalisant le chiffrement et qu’on est donc capable de lui transmettre en entrée les données de notre choix. Notre algorithme du DES à 4 tours étant vulnérable à ce type d’attaque on est en mesure de récupérer la clé ayant servi au chiffrement des données.
	
	\section{Analyses préalables}
		Dans le cas d'un DES à n tours, soit $\Delta X$ la différence entre deux messages en entrée du premier tour et $\Delta Y$ la différence observée en sortie du n-1 tours, le couple ($\Delta X$, $\Delta Y$) est appelé un \textit{différentiel}.\\
Lors de cette phase d'analyse nous allons chercher à construire ce que l'on appelle une caractéristique différentielle du chiffre, c'est à dire une façon très probable dont peut se propager un différentiel à travers tous les tours du chiffrement. \\

L'algorithme que nous allons analyser présente la structure décrite par la figure~\ref{des4tours}.\\
Une différence ne se propage pas uniformément à travers tous les tours, c'est-à-dire $\Delta X \ne \Delta Y$. Il s'agit donc tout d'abord de repérer et d'analyser la source de la variation de cette différence :

\begin{description}
	\item [Les multiples permutations des bits.] Cependant, ces permutations sont toujours les mêmes quels que soient les tours, et par conséquent elles sont inversibles et prévisibles.
	\item [Les expansions.] Néanmoins, ces expansions sont également les mêmes à chaque tour et sont donc prévisibles.
	\item [Les boîtes-S.] De sorties identiques peuvent provenir d'entrée différentes, c'est donc une source d'incertitudes.
\end{description}

La clé étant identique pour chiffrer tous les messages lors d'une cryptanalyse différentielle, sa différence entre deux messages chiffrés est nulle et n'a donc pas d'influence sur les différentiels.\\
Seule l'analyse des boîtes-S est donc nécessaire pour construire une caractéristique différentielle du chiffre.
		
		\subsection{Analyse des boites-S}
			Les boîtes-S prennent 6 bits en entrée et 4 bits en sortie. Chaque boîte-S est représentée par un tableau dont le contenu indique les 4 bits de sortie. Le choix de la case à lire dans le tableau s'effectuera comme suit : Les 2 bits des extrémités une fois concaténés valent l'indice de la colonne et les 4 bits intérieurs valent l'indice de la ligne.\\
			
			Lors de l'analyse des boîtes-S il s'agira d'évaluer pour chaque boîte-S la probabilité qu'une différence apparaisse en sortie pour une différence en entrée donnée.\\
			On commence donc par générer dans un vecteur \texttt{X[]} toutes les valeurs possibles pour l'entrée : 6 bits en entrée donc $2^6 = 64$ valeurs possibles. On passe ensuite toutes les valeurs de X dans la boîte-S en cours d'analyse et on rentre le résultat dans un vecteur \texttt{Y[]}.
			Pour tous les $\Delta X$ obtenus en combinant toutes les valeurs du vecteur entre elles on note la fréquence d'apparition d'un $\Delta Y$ entre les sorties correspondantes.
			
			\begin{figure}[h]
				\centering
				\includegraphics[scale=0.5]{Sbox1.png}
				\caption{Distribution des différentiels dans la première boîte-S}
				\label{sbox1}
			\end{figure}
			
On effectue cette analyse pour les 8 boîtes-S et on constate que pour toutes les boîtes-S il existe certains différentiels ($\Delta X$,$\Delta Y$) qui ont une plus forte probabilité d'apparition. Par exemple, dans la première boîte-S analysée en figure~\ref{sbox1} pour $(000011,0000)$ on a $p = \frac{14}{64}$. Cette inégale répartition de la probabilité constitue la faiblesse de l'algorithme du DES et c'est ce qui le rend vulnérable à la cryptanalyse différentielle.
		
		\subsection{Recherche d'un différentiel}
		\label{recherchedif}
			On connaît maintenant pour chaque boîte-S la probabilité d'apparition d'un certain $\Delta Y$ en sortie pour un $\Delta X$ donné. On peut donc prévoir avec une forte probabilité la variation de la différence à travers un tour.\\

Pour choisir le différentiel entre l'entrée $\Delta R$ et la sortie $\Delta L$ d'un tour on fait passer toutes les valeurs possibles pour $\Delta R$ dans ce tour. Pour chaque $\Delta X$ qui arrive en entrée des boîtes-S on fait sortir le $\Delta Y$ le plus probable et on note sa probabilité de sortie. Toutes les autres transformations du tour étant prévisibles, la probabilité imposée par les boîtes-S établira la probabilité d'apparition de la valeur en sortie du tour.\\
$2^{32}$ valeurs de $\Delta R$ sont à tester et pour chaque valeur il faut chercher dans la table des différentiels de chaque boîte-S la probabilité la plus élevée. La recherche du différentiel a donc nécessité quatre heures de calcul. Pour ne pas abîmer nos machines en les maintenant sur une longue période à 100\% d'utilisation du CPU nous avons fait appel au service de cloud computing proposé par Amazon Web Services (AWS). \\

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.5]{differentiel.png}
	\caption{Recherche d'un différentiel via AWS}
	\label{differentiel}
\end{figure}
		
On obtient ainsi le différentiel $(0x040000; 0x400800)$ entre l'entrée et la sortie du premier tour avec une probabilité qui vaut $p = \frac{16}{64} = 0,25$. Une fois étendu à trois tours \footnote{Pour étendre le calcul de la propabilité à la sortie du n-ième tour on reproduit le mécanisme du chiffrement de la figure~\ref{des4tours} dans lequel on retire le XOR avec la clé} la probabilité d'apparition de ce différentiel vaut $p = \frac{16}{64} \times 1 \times \frac{16}{64} = 0,0625$.
	
	\section{Cryptanalyse}
		Bien que le différentiel déterminé en partie \ref{recherchedif} nous semble être le plus probable nous n'avons aucune certitude sur sa véracité. Nous allons donc utiliser un différentiel connu et utilisé en cryptanalyse depuis de nombreuses années\footnote{Utilisation de ce différentiel par Anne Cantaud \cite{cantaud} et par Bart Preneel \cite{preneel}} pour effectuer la suite de cette partie : $\Delta X = 0x60000000$ et $\Delta Y = 0x00808200$, avec $\Delta X$ la différence en entrée du premier tour et $\Delta Y$ la différence en sortie du 3\ieme{} tour. La probabilité pour que ce $\Delta Y$ apparaisse en sortie du 3ème tour vaut $p = \frac{7}{32} \times 1 \times \frac{7}{32} = 0,04785$
		
		\begin{figure}[h]
			\centering
			\includegraphics[scale=0.5]{DES.png}
			\caption{Probabilité de propagation d'un différentiel à travers quatre tours}
			\label{delta4tours}
		\end{figure}
		
		\subsection{Génération des couples clairs/chiffrés}
			On choisit 50 couples clairs X et X' de sorte qu'ils aient en entrée du premier tour $X \oplus X' = \Delta X$. Pour cela on génère un premier message X sur 64 bits. On lui applique la permutation initiale de notre algorithme du DES, puis on génère le X' tel que $X' = X \oplus \Delta X$.\\
On crypte ensuite ces X et X' pour obtenir les couples chiffrés Y et Y' correspondants.
			
		\subsection{Récupération des bits de la clé}
			Comme vu en première partie, pour décrypter un message chiffré avec le DES on change l'ordre des clés. Une fois effectué le 4\ieme{} tour il existe donc une clé permettant de décrypter ce tour pour remonter au tour précédent. Si la clé testée nous permet de remonter à la fin du tour 3 on s'attend donc à trouver un $\Delta Y$ entre nos deux chiffré de valeur $0x0080820$.\\
			
	Une fois passée à travers la matrice d'expansion la différence à l'entrée du tour 4 ($0x00808200$) va influer sur les bits 11, 13, 23, 25 et 33 de la clé. Pour décrypter ce tour on construit donc un tour du DES dans lequel on effectuera un XOR sur ces 5 bits. On aura donc $2^5$ combinaisons différentes à tester sur les 50 couples en entrée.\\
	
	\begin{figure}[h]
		\centering
		\includegraphics[scale=0.5]{DES1tour.png}
		\caption{Un tour du DES}
		\label{des1tour}
	\end{figure}
			
			Sur 50 couples générés, d'après la probabilité théorique établie précédemment, on s'attend à retrouver en sortie du tour $Y \oplus Y' = \Delta Y$ pour environs 2 couples si la partie de la clé testée s'avère exacte.
			
			\begin{center}
				$50 * p = 50 * 0,04785 \approx 2$
			\end{center}
			
			Pour déterminer les bits restants il ne reste plus qu'à prendre un cryptogramme à 3 tours et le repasser dans un tour du DES en faisant varier les $48-5$ autres bits qui n'ont pas encore été trouvés. Dès que l'on retombe sur le cryptogramme à 4 tour de départ on est donc en possession de la clé du dernier tour.\\
			
			Cette dernière partie n'a pas pu être achevée car aucune paire de cryptogrammes de 3\ieme{} tour n'a présenté le $\Delta Y$ attendu.
			
%%% CONCLUSION %%%
\chapter*{Conclusion}
\addcontentsline{toc}{chapter}{Conclusion}

	De la recherche documentaire en passant par l'utilisation d'outils de travail collaboratif jusqu'à la production de documents professionnels, la réalisation de ce projet nous a permis d'acquérir une vraie méthode de recherche. Nous avons dû apprendre à gérer un planning étendu sur tout un semestre, ce que nous n'avions pas eu à faire lors de nos précédents projets.\\
Le choix du langage C qui semblait convenir pour la partie reproduction de l'algorithme du DES a généré des difficultés lors de la partie Cryptanalyse du fait de l'absence de bibliothèques de gestion de listes.\\

	Pour poursuivre et améliorer la partie Cryptanalyse après avoir identifié la source de notre erreur, il serait intéressant de proposer à l'utilisateur l'importation d'un fichier de couples clairs/chiffrés au début du programme. Une fois la clé du dernier tour récupérée on peut également envisager la mise en place d'un affichage qui déchiffre chaque message contenu dans le fichier.\\
	
	La cryptanalyse différentielle prouve son efficacité sur un algorithme de DES à quatre tours en permettant de retrouver la clé du dernier tour dans un court délai. Cependant, la probabilité d'apparition d'un différentiel diminuant avec l'augmentation du nombre de tours, cette cryptanalyse appliquée au DES réel (à 16 tours) se révèle plus coûteuse que la recherche exhaustive. Dans ce cas on se tournera plutôt vers la cryptanalyse linéaire\cite{lineaire}.

%%% Biblio %%%
\begin{thebibliography}{5}
	\addcontentsline{toc}{chapter}{Bibliographie}
	\bibitem{grabbe}
		J. Orlin Grabbe,
		\emph{The DES Algorithm Illustrated}.\\
		http://page.math.tu-berlin.de/~kant/teaching/hess/krypto-ws2006/des.htm\\
	
	\bibitem{martin}
		Bruno Martin,
		\emph{Codage, cryptologie et applications}.\\
		29 avril 2004\\
    
	\bibitem{cantaud}
		Anne Cantaud,
		\emph{Cryptanalyse des chiffrements à clef secrète par blocs}.\\
		https://www.rocq.inria.fr/secret/Anne.Canteaut/Publications/Canteaut02a.pdf,\\
		INRIA - Projet CODES,\\
		Mars 2002\\
	
	\bibitem{preneel}
		Bart Preneel,
		\emph{Block ciphers}.\\
		Katholieke Universiteit Leuven, Belgium,\\
		Fevrier 2003\\
		
	\bibitem{lineaire}
		Wikipédia,
		\emph{Cryptanalyse linéaire}.\\
		https://fr.wikipedia.org/wiki/Cryptanalyse\_lin\%C3\%A9aire\\
\end{thebibliography}

%%% Table des illustrations %%%
\listoffigures
\addcontentsline{toc}{chapter}{Table des illustrations}

\end{document}          
