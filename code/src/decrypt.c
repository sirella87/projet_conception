#include <stdio.h>

#include "../inc/vecteurs.h"
#include "../inc/chaines.h"
#include "../inc/commandes.h"
#include "../inc/des.h"

int main (int argc, char *argv[])
{
	header(argv[0]);
	
	int nb_tours = 16;
	
	int texte_binaire[1024];
	int cle_binaire[64];
	int taille_texte_binaire = verifArguments(argc, argv, texte_binaire, cle_binaire);
	
	if (taille_texte_binaire < 0) { /* Erreur dans les arguments -> arrêt du programme */ 
		return -1;
	}
	
	//printf("Chaine hexa en binaire : ");
	//afficherTab(texte_binaire, taille_texte_binaire);
	
	int donnees[64];
	int debut_bloc = 0;
	char chaine_crypt[64];
	int resultat[64];
	
	printf("\n-> Texte déchiffré : ");
	
	int i,j;
	for (i = 0; i < taille_texte_binaire/64; i++) { // une iteration par bloc de 64 bits 
		//printf("\n\nCRYPTAGE DU BLOC %d\n\n", i);
		for (j = 0; j < 64; j++) {
			donnees[j] = texte_binaire[debut_bloc+j];
		}
		
		des(0, donnees, resultat, cle_binaire, nb_tours);
		binaireChaine(resultat,64);
		
		debut_bloc += 64;
	}
	
	printf("\n----------- FIN ------------- \n");
	return 0;
}
