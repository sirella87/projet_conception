#include <stdio.h>

#include "../inc/matrices.h"
#include "../inc/vecteurs.h"
#include "../inc/chaines.h"
#include "../inc/des.h"

//fonction pour le xor entre a et b dans c
int XOR(int a, int b)
{
	if (( a == 0 && b == 0 ) || ( a == 1 && b == 1 )) { return 0; }
	if (( a == 1 && b == 0) || (a == 0 && b == 1)) { return 1; }
}

//---------------------------CLE-----------------------------
//premiere reduction de clef 64->56bits
void reductionClef1(int K[]){
	int tmp[56];
	int i;
	for(i=0; i<56; i++)
	{
		tmp[i]=K[matriceReduc1[i]-1];
	}
	initialiser(K,56);
	copieTab(K,tmp,56);
}
// fonction de rotation de la clef K en fonction du tout n
void rotation(int K[], int n){
	int tmp[28];
	int i;
	for(i=1;i<28;i++)
	{
		tmp[i-1]=K[i];
	}
	tmp[27]=K[0];
	copieTab(K,tmp,28);
}

// deuxieme fonction de reduction de clef 56->48bits
void reductionClef2(int K[]){
                 
    int tmp[48];
	int i;
	for(i=0; i<48; i++)
	{
		tmp[i]=K[matriceReduc2[i]-1];
	}
	
	copieTab(K,tmp,48);
	
}
//algo de calcul des 4 sous clefs
//n étant le tour de boucle où l'on se trouve
void G(int K[],int clefTour[48][16], int nb_tours, int crypt)
{
	int C0[28];
	int D0[28];
	int C1[28];
	int D1[28];
		
	DiviseDonnees(K,C0,D0,56); //division de la clef en parte droite et gauche
	int i,j;
	for(j=0; j<nb_tours; j++) // nombre de rounds
	{
		int nb_rotations = matriceRotation[j]; // on recupere le nombre de rotation en fonction du numero de tour
		for (i = 0; i < nb_rotations; i++) {
			rotation(C0,i);//rotation de la partie droite
			rotation(D0,i);//rotation de la partie gauche
		}
		copieTab(C1,C0,28); // on copie C0 dans C1
		copieTab(D1,D0,28); // on copie D0 dans D1
		
		concatene(C1,D1,K,56); // concatenation des parties gauche et droite
		
		reductionClef2(K);//réduction de la clef de 56 à 48 bits plus mélange
		
		//on copie la clef dans le tableaux des clefs
		int m;
		for(m = 0; m<48; m++) 
		{
			if (crypt == 1) // On crypte
				clefTour[m][j]=K[m];
			else // On décrypte
				clefTour[m][nb_tours-j-1]=K[m];
		}
		/*
		if (j == nb_tours - 1) {
			printf("Clé du dernier tour : ");
			afficherTab(K, 48);
			printf("\n");
		}	*/
	}
	/*
	//on affiche le tableau de clef
	int m;
	printf("verif tableau \n");	
	for(j = 0; j < nb_tours; j++){
		for(m = 0; m<48; m++){
			printf("%d",clefTour[m][j]);
		}
	printf("\n");
	}
	*/
}
//---------------------------TEXTE---------------------------
//premiere permutation des bits du texte

void PermutInitiale(int w[])
{
	int tmp[64];
	int i;
	for(i=0; i<64; i++)
	{
		tmp[i]=w[matriceInitiale[i]-1];
	}
	copieTab(w,tmp,64);
}

void inversePermutInitiale (int mat[64])
{
	int tmp[64];
	int i;
	for(i = 0; i < 64; i++) {
		tmp[matriceInitiale[i]-1] = mat[i];
	}
	
	copieTab(mat,tmp,64);
}

void P_Permutation(int w[])
{
	int tmp[32];
	int i;
	for(i=0; i<32; i++)
	{
		tmp[i]=w[matriceP[i]-1];
	}
	copieTab(w,tmp,32);
}
void PermutFinale(int w[])
{
	int tmp[64];
	int i;
	for(i=0; i<64; i++)
	{
		tmp[i]=w[matriceFinale[i]-1];
	}
	copieTab(w,tmp,64);
}
void inversePermutFinale(int mat[64])
{
	int tmp[64];
	int i;
	for(i = 0; i < 64; i++) {
		tmp[matriceFinale[i]-1] = mat[i];
	}
	
	copieTab(mat,tmp,64);
}
void EtendreDonnees(int R[])
{
	int tmp[48];
	int i;
	for(i=0; i<48; i++)
	{
		tmp[i]=R[matriceExp[i]-1];
	}
	copieTab(R,tmp,48);
}

void expansion(int mat[32], int matEtendue[48])
{
	int i;
	for(i = 0; i < 48; i++) {
		matEtendue[i] = mat[matriceExp[i]-1];
	}
}

void inverseExpansion(int matEtendue[48], int mat[32])
{
	int i;
	for(i = 0; i < 48; i++) {
		mat[matriceExp[i]-1] = matEtendue[i];
	}
}

void SBoite(int entree[6], int sortie[4], int numSbox)
{
	int lig[2], col[4];
	
	// Ligne
	lig[0] = entree[0];
	lig[1] = entree[5];
	int ligne = bin_to_dec(lig, 2);
	//printf("lig : %i\n", ligne);
	
	// Colonne
	col[0] = entree[1];
	col[1] = entree[2];
	col[2] = entree[3];
	col[3] = entree[4];
	int colonne = bin_to_dec(col, 4);
	//printf("col : %i\n", colonne);
	
	dec_to_bin(sboites[numSbox][ligne][colonne], 4, sortie);
}

void SBox(int R[])
{
	int col[4], ligne[2],i ,j, indexL=0, indexC=0, nbBloc = 0, m;
	for(i=0; i<48; i+=6){
		for(j=0; j<6; j++){
			if(j==0 || j==5){
				ligne[indexL] = R[i+j];
				indexL ++;
			}
			else {
				col[indexC] = R[i+j];
				indexC ++;
			}
		}
		int colonne = bin_to_dec(col,4);
		int L = bin_to_dec(ligne,2);
		int resultat = sbox[nbBloc][L][colonne];
		int nombre[4];
		dec_to_bin(resultat,4,nombre);
		
		for(m=0; m<4; m++)
		{
			R[nbBloc*4+m]=nombre[m];
		}
		indexL = 0;
		indexC = 0;
		nbBloc ++;
	}
			
}

void des (int crypt, int donnees[64], int resultat[64], int cle_binaire[64], int nb_tours)
{
	/* Si crypt = 1 on crypte : Clés dans l'ordre
	 * Si crypt = 0 on décrypte : Clés dans le sens inverse
	 */
	 
	//---------CLEF--------------
	int K[64];
	//int K[64]={0,0,0,1,0,0,1,1,0,0,1,1,0,1,0,0,0,1,0,1,0,1,1,1,0,1,1,1,1,0,0,1,1,0,0,1,1,0,1,1,1,0,1,1,1,1,0,0,1,1,0,1,1,1,1,1,1,1,1,1,0,0,0,1};
	copieTab(K, cle_binaire, 64);
	// clef K initiale
	reductionClef1(K);//réduction de la clef initiale de 64 à 56 bits plus mélange
	//printf("affichage de la clef après réduction et mélange :\n");
	//afficherTab(K,56);
	// on genere les 4 clefs
	int clefTour[48][16]; // tableau qui va contenir les 4 clefs des 4 tours
	G(K,clefTour, nb_tours, crypt);//fonction pour déterminer les 4 clefs
	//-------------------------
	//--------TEXTE-------------
	int i; 
	int L0[32]; // pour stocker la partie gauche du texte
	int R0[48];// pour stocker la partie droite du texte
	int L1[32]; 
	int R1[48];
	//printf("Texte en binaire non mélangé :\n");
	//afficherTab(donnees,64);
	
	PermutInitiale(donnees); // permutation initiale
	//printf("Texte en binaire mélangé après la permutation initiale :\n");
	//afficherTab(donnees,64);

	DiviseDonnees(donnees,L0,R0,64);//division du texte en partie G et partie D
	//printf("affichage de la partie gauche du texte :\n");
	//afficherTab(L0,32);
	
	//printf("affichage de la partie droite du texte :\n");
	//afficherTab(R0,32);
	int nbround;
	for(nbround = 0; nbround < nb_tours ; nbround ++) {
		//printf("Tours n°%d\n",nbround+1);
		//printf("---------------------------------------------\n");
		
		copieTab(L1,R0,32); //L1 recoit R0 
		EtendreDonnees(R0);//fonction pour etendre R de 32 a 48bits
		//printf("affichage de la partie droite après étendue : \n");
		//afficherTab(R0,48);
		
		int L00[48];
		copieTab(L00,L0,32); // je copie L0 dans un L00 pour le bug
		int tmp[48];
		for(i=48; i>=0; i--)
		{
			tmp[i] = XOR(clefTour[i][nbround],R0[i]);
		}
		
		copieTab(R0,tmp,48);
		//printf("apres le xor entre la clef et la partie droite :\n");		
		//afficherTab(R0,48);
		
		SBox(R0);
		//printf("apres les sbox \n");
		//afficherTab(R0,32);
		
		P_Permutation(R0);
		//printf("apres la P Permutation :\n");
		//afficherTab(R0,32);
		
		for(i=0; i<=32; i++)
		{
			R1[i] = XOR(R0[i],L00[i]);// R1 recoit le xor entre L0 et R0
		}
		//printf("apres le deuxieme xor \n");
		//afficherTab(R1,32);
		
		
		copieTab(L0,L1,32); //L0 recoit L1 
		copieTab(R0,R1,32); //R0 recoit R1 */
		
		//printf("\nR%d : ",nbround+1);
		//afficherTab(R1,32);
	
		//printf("\nL%d : ",nbround+1);
		//afficherTab(L1,32);
		//printf("---------------------------------------------\n");
	}
	
	concatene(R1,L1,resultat, 64);
	
	PermutFinale(resultat);
	//printf("\nRésultat en binaire : ");
	//afficherTab(resultat,64);

	/* après chaque itération du for on affiche le résultat en hexa 
	 * et on passe au bloc suivant
	 */
}

