#include <stdio.h>

// fonction pour initialiser un tableau à '9'. je sais pas si ça nous servira
void initialiser(int Tab[], int N)
{
	int i;
	for(i=0; i<N; i++)
	{
		Tab[i]=9;
	}
}

void copieTab(int Tab1[],int Tab2[],int taille)
{
	// Copie un tableau Tab2 dans un tableau Tab1 de taille "taille"
	int i;
	for(i=0; i<taille; i++)
	{
		Tab1[i]=Tab2[i];
	}
}

void afficherTab(int w[], int n)
{
	// Affichage d'un tableau w de taille n
	int i;
	for(i = 0; i < n; i++) {
		if ( (n == 64) || (n == 32) ) {
			if (i%4 == 0)
				printf(" ");
			printf("%d", w[i]);
		}
		
		else if (n == 48) {
			if (i%6 == 0)
				printf(" ");
			printf("%d", w[i]);
		}
		
		else
			printf("%d", w[i]);
	}
}

void concatene(int Tab1[], int Tab2[],int Tab3[],int taille)
{
	// concatene Tab1 et Tab2 de taille "taille/2" dans un tableau Tab3 de taille "taille"
	int i;
	for(i=0;i<taille/2;i++)
	{
		Tab3[i]=Tab1[i];
	}
	for(i=taille/2;i<taille;i++)
	{
		Tab3[i]=Tab2[i-taille/2];
	}
}

// division d'un tableau en partie gauche et droite
void DiviseDonnees(int tab[],int L[],int R[],int taille)
{
	//L recoit la partie gauche de tab
	//R recoit la partie droite de tab
	int i;
	for(i = 0; i<taille/2 ; i++)
	{
		L[i]=tab[i];
	}
	for(i = taille/2; i<taille ; i++)
	{
		R[i-taille/2]=tab[i];
	}
}
