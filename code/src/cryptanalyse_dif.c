#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#include "../inc/crypt.h"
#include "../inc/cryptanalyse_dif.h"
#include "../inc/vecteurs.h"
#include "../inc/chaines.h"
#include "../inc/des.h"

int probaSbox (int vectTabDiff[8][64][16], int numSbox, int dMEtendu[48], int dM[32]) {
	int dX[6], dY[4];
	int numBit = 0;
	int i, j;
	for (i = numSbox * 6; i < numSbox * 6 + 6; i++) {
		dX[numBit] = dMEtendu[i]; 
		numBit++;
	}
	/*
	printf("\ndX : ");
	afficherTab(dX, 6);
	printf("\n");
	*/
	int max = 0;
	int dXDec = bin_to_dec(dX, 6);
	
	if (dXDec == 0) {
		max = 64;
		for (i = numSbox * 4; i < numSbox * 4 + 4; i++) {
			dM[i] = 0;
			dec_to_bin(0, 4, dY);
		}
	}
	
	else {
		for (i = 0; i < 15; i++) {
			//printf("vectTabDiff[%i][%i][%i] = %i\n", numSbox, dXDec, i, vectTabDiff[numSbox][dXDec][i]);
			if (max < vectTabDiff[numSbox][dXDec][i]) {
				max = vectTabDiff[numSbox][dXDec][i];
				//printf("max : %i à (%i,%i)\n", max, dXDec, i);
				dec_to_bin(i, 4, dY);
			
				numBit = 0;
				for (i = numSbox * 4; i < numSbox * 4 + 4; i++) {
					dM[i] = dY[numBit]; 
					numBit++;
				}
			}
		}
	}
	/*
	printf("dY : ");
	afficherTab(dY, 4);
	printf("\n");
	
	printf("proba = %i/64\n", max);
	*/
	return max;
}

int genererCouples (int nbCouples, int dEntree[64], int dSortie[64], int X[1000][64], int Y[1000][64]) {
	// Génère les couples (clair, chiffré) et les mets dans le fichier
	srand(time(NULL));
	
	int cle[64] = {
		1,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,1
	};
	
	int tmp[64];
	int XPermute[64];
		
	int i, k;
	for (i = 0; i < nbCouples*2; i+=2) {
		for (k = 0; k < 64; k++) // Générateur d'alea du bled
			X[i][k] = rand() % 2;
			
		// On descend après la permutation initiale pour tester l'entrée du premier tour
		copieTab(XPermute, X[i], 64);
		PermutInitiale(XPermute); 
		
		for (k = 0; k < 64; k++)
			X[i+1][k] = XOR(XPermute[k], dEntree[k]);
		
		for (k = 0; k < 64; k++)
			tmp[k] = XOR(X[i+1][k], XPermute[k]);
			
		// On inverse la permutation initiale pour remonter à l'entrée du DES
		inversePermutInitiale(X[i+1]);
		
		/*
		printf("\n\nCouple n°%i\n", i/2);
		
		// On affiche les X
		printf("X%i  : ", i/2);
		afficherTab(X[i], 64);
		printf("\n");
		
		printf("X%i'  : ", i/2);
		afficherTab(X[i+1], 64);
		printf("\n");
		
		printf("XOR X et X' : ");
		afficherTab(tmp, 64);
		printf("\n");
		*/
		
		// On chiffre X et X' sur 4 tours
		des (1, X[i], Y[i], cle, 4);
		des (1, X[i+1], Y[i+1], cle, 4);
		
		// On inverse la dernière permutation pour Y et Y'
		inversePermutFinale(Y[i]);
		inversePermutFinale(Y[i+1]);
		
		/*
		// On affiche les Y
		printf("Y%i : ", i/2);
		afficherTab(Y[i], 64);
		printf("\n");
		
		printf("Y%i' : ", i/2);
		afficherTab(Y[i+1], 64);
		printf("\n");*/
	}
}

void afficherTabDiff(int numSbox, int vectTabDiff[8][64][16]) {
	int i, j, k;

	// Chaque Sbox prend 6 bits en entrée et 4 bits en sortie
	// On a donc 2^6 valeurs de X différentes à calculer
	int X[64][6];
	for (i = 0; i < 64; i++)
		dec_to_bin(i, 6, X[i]);
	
	/* DEBUG
	for (i = 0; i < 64; i++) {
		printf("X[%i] : ", i);
		afficherTab(X[i], 6);
		printf("\n");
	}
	printf("\n");*/
	
	// On passe chaque X dans la Sbox pour avoir le Y correspondant
	int Y[64][4];
	for (i = 0; i < 64; i++) {
		SBoite(X[i], Y[i], numSbox);
		//printf("Y[%i] : ", i);
		//afficherTab(Y[i], 4);
		//printf("\n");
	}
		
	// On génère la table des différentiels
	int tabDiff[64][16]; // 64 dX et 15 dY
	for (i = 0; i < 64; i++) {
		for (j = 0; j < 16; j++) {
			vectTabDiff[numSbox][i][j] = 0;
		}
	}
	
	int tmpX[6], tmpY[6];
	int dX, dY;
	for (i = 0; i < 64; i++) {
		for (j = 0; j < 64; j++) {
			// On met chaque différence dans des tableaux temporaires et on calcule leurs valeurs
			for (k = 0; k < 6; k++)
				tmpX[k] = XOR(X[i][k], X[j][k]);
			dX = bin_to_dec(tmpX, 6);
			
			for (k = 0; k < 4; k++)
				tmpY[k] = XOR(Y[i][k], Y[j][k]);
			dY = bin_to_dec(tmpY, 4);
			
			// On incrémente la case du tableau aux coordonnées dX et dY
			//printf("tabDiff[%i][%i] = %i + 1 = %i\n", dX, dY, tabDiff[dX][dY], tabDiff[dX][dY] + 1);
			vectTabDiff[numSbox][dX][dY] = vectTabDiff[numSbox][dX][dY] + 1;
		}
	}
	
	printf("Table des différentiels de la Sbox n°%i : \n", numSbox);

	for (i = 0; i < 64; i++) {
		for (j = 0; j < 16; j++) {
			printf("%i\t", vectTabDiff[numSbox][i][j]);
		}
		printf("\n");
	}
}

int main (void) {
	char choix;
	
	int vectTabDiff[8][64][16];
	
	/* --- Analyse des sbox et ajout dans le vecteur --- */
	printf("\nLancer l'analyse des sbox ? [o/n] ");
	scanf("%c", choix);
	
	//if (choix == 'o') {
		afficherTabDiff(0, vectTabDiff);
		afficherTabDiff(1, vectTabDiff);
		afficherTabDiff(2, vectTabDiff);
		afficherTabDiff(3, vectTabDiff);
		afficherTabDiff(4, vectTabDiff);
		afficherTabDiff(5, vectTabDiff);
		afficherTabDiff(6, vectTabDiff);
		afficherTabDiff(7, vectTabDiff);
	//}

	int dR[32], dL[32];
	int probaMax_dR[32], probaMax_dL[32];
	
	int tmp[32], tmp2[6];
	int dM[32];
	int dMEtendu[32];
	int dX;
	float proba;
	float probaTour;
	float probaFinale;
	float probaMax = 0.0;
	int i, j;
	
	/* --- Analyse du premier tour (long) --- */
	printf("\nLancer l'analyse du premier tour ? [o/n] ");
	scanf("%c", choix);
	
	if (choix == 'o') {
		unsigned int k;
		unsigned int maxK = 4294967295; // 4294967296;
		for (k = 0; k < maxK; k++) { // Pour chaque différence dR
			dec_to_bin(k, 32, dR);
			dec_to_bin(0, 32, dL);
			probaFinale = 1.0;
		
			expansion(dR, dMEtendu);
		
			probaTour = 1.0;
			for (j = 0; j < 8; j++) {
				proba = probaSbox(vectTabDiff, j, dMEtendu, dM) / 64.0;
				probaTour *= proba;
			}
		
			P_Permutation(dM);
		
			for (j = 0; j < 32; j++)
				tmp[j] = XOR(dM[j], dL[j]);
			copieTab(dL, tmp, 32);
		
			probaFinale = probaTour;
		
			if (k%1000000 == 0) // affiche l'avancement ~ toutes les secondes
				printf("Avancement : %.3f % (K = %u)\n", (float)k / (float)maxK * 100.0, k);
		
			if ( (probaMax < probaFinale) && (probaFinale != 1) ) {
				dec_to_bin(k, 32, dR);
			
				probaMax = probaFinale;
				copieTab(probaMax_dR, dR, 32);
				copieTab(probaMax_dL, dL, 32);
			
				printf("\ndR = ");
				afficherTab(dR, 32);
				printf("\n");
				printf("dL = ");
				afficherTab(dL, 32);
				printf(" --> p = %.20f \n", probaFinale);
			}
		}
	
		printf("\nProba max de %.20f", probaMax);
		printf(" trouvée pour dR = ");
		afficherTab(probaMax_dR, 32);
		printf(" et dL = ");
		afficherTab(probaMax_dL, 32);
		printf("\n");
	}
	
	/* On trouve dR = 0000 0100 0000 0000 0000 0000 0000 0000
	 * et dL = 0100 0000 0000 1000 0000 0000 0000 0000
	 * et p = 0.25 */
	
	printf("On prend des valeurs connues : ");
		printf("\ndR = 0110 0000 0000 0000 0000 0000 0000 0000\n");
		int dRtmp[32] = {
			0,1,1,0,
			0,0,0,0,
			0,0,0,0,
			0,0,0,0,
			0,0,0,0,
			0,0,0,0,
			0,0,0,0,
			0,0,0,0
		};
		copieTab(dR, dRtmp, 32);
		printf("dL = 0000 0000 1000 0000 1000 0010 0000 0000\n");
		int dLtmp[32] = {
			0,0,0,0,
			0,0,0,0,
			1,0,0,0,
			0,0,0,0,
			1,0,0,0,
			0,0,1,0,
			0,0,0,0,
			0,0,0,0
		};
		copieTab(dL, dLtmp, 32);
		printf("p --> 14/64\n");
		
	int dEntree[64];
	concatene(dLtmp, dRtmp, dEntree, 64);
	
	printf("\n==> dEntree : ");
	afficherTab(dEntree, 64);
	printf("\n");
	
	/* A partir des résultats précédents on regarde la propagation sur 4 tours */
	int nbTours = 4;
	probaFinale = 1.0;
	for (i = 0; i < nbTours; i++) { // Pour chaque tours
		printf("\nDebut du tour %i : \n", i + 1);
		if (i != 0) {
			copieTab(tmp, dR, 32);
			copieTab(dR, dL, 32);
			printf("dR = dL = ");
			afficherTab(dR, 32);
			copieTab(dL, tmp, 32);
			printf("\ndL = dR = ");
			afficherTab(dL, 32);
			printf("\n");
		}
		
		printf("dR = ");
		afficherTab (dR, 32);
	
		printf(" --> Expansion --> ");
		expansion(dR, dMEtendu);
		afficherTab(dMEtendu, 48);
		printf("\n");
		
		if (i == nbTours - 1) {// si tour 4
			printf("delta sur la clé du dernier tour = ");
			afficherTab(dMEtendu, 48);
		}
		
		if (i != nbTours - 1) { // si tour < 4
			printf("dM avant sbox = ");
			afficherTab(dMEtendu, 48);
		
			printf(" --> SBOX --> ");
			// On regarde avec quelle probabilité la différence se propage à travers les sbox
			probaTour = 1.0;
			for (j = 0; j < 8; j++) {
				proba = probaSbox(vectTabDiff, j, dMEtendu, dM) / 64.0;
				probaTour *= proba;
			}
			printf("\ndM après sbox = ");
			afficherTab(dM, 32);
			printf(" (p = %f)", probaTour);
			printf("\n");
	
			printf(" --> Permutation --> ");
			P_Permutation(dM);
			afficherTab(dM, 32);
			printf("\n");

			printf("dL = dM ^ dL = ");
			afficherTab(dM, 32);
			printf(" ^ ");
			afficherTab(dL, 32);
			printf(" = ");
			for (j = 0; j < 32; j++)
				tmp[j] = XOR(dM[j], dL[j]);
			copieTab(dL, tmp, 32);
			afficherTab(dL, 32);
			printf("\n");
		
			probaFinale *= probaTour;
		}
	}
	
	printf("\n\nProba que ce schéma se réalise : %f\n", probaFinale);
	
	int dSortie[64];
	concatene(dR, dL, dSortie, 64);
	
	//inversePermutInitiale(dEntree);
	printf("\n==> dEntree premier tour : ");
	afficherTab(dEntree, 64);
	printf("\n");
	
	//PermutFinale(dSortie);
	printf("==> delta recherché en entrée du 4e tour : ");
	afficherTab(dR, 32);
	printf("\n\n");
	
	// Clé du dernier tour :  000000 000000 000000 000001 000000 000000 000000 000000
	// delta sur la clé du dernier tour =  000000 000001 010000 000001 010000 000100 000000 000000

	int X[1000][64];
	int Y[1000][64];
	
	int partieDroite[2][48];
	int partieGauche[2][48];
	
	int cle[5];
	int sortieXOR[2][48];
	
	genererCouples (50, dEntree, dSortie, X, Y);
	
	// Pour chaque possibilité des 5 bits à changer pour la clé
	int k;
	for (i = 0; i < 32; i++) { // 2^5 valeurs
		dec_to_bin(i, 5, cle);
		printf("Bits testés : ");
		afficherTab(cle, 5);
		printf("\n");
		
		for (j = 0; j < 100; j+=2) { // On utilise la clé sur tous les couples
			DiviseDonnees(Y[j], partieGauche[0], partieDroite[0], 64);
			DiviseDonnees(Y[j+1], partieGauche[1], partieDroite[1], 64);
			/*printf("1 partie droite : ");
			afficherTab(partieDroite[0], 32);
			printf("\n");
			printf("1 partie droite' : ");
			afficherTab(partieDroite[1], 32);
			printf("\n");*/
			/*printf("Y: ");
			afficherTab(Y[j], 64);
			printf("\n");
			afficherTab(partieGauche[0], 32);
			afficherTab(partieDroite[0], 32);
			printf("\n");
			printf("Y': ");
			afficherTab(Y[j+1], 64);
			printf("\n");
			afficherTab(partieGauche[1],32);
			afficherTab(partieDroite[1], 32);
			printf("\n");*/
		
			EtendreDonnees(partieGauche[0]);
			EtendreDonnees(partieGauche[1]);
		
			// On XOR avec la clé
			copieTab(sortieXOR[0], partieGauche[0], 48);
			copieTab(sortieXOR[1], partieGauche[1], 48);
			/*printf("sortie xor: " );
			afficherTab(sortieXOR[0], 48);
			printf("\npartie Gauche : ");
			afficherTab(partieGauche[0], 48);
			printf("\n");
			printf("sortie xor' : " );
			afficherTab(sortieXOR[1], 48);
			printf("\npartie Gauche' : ");
			afficherTab(partieGauche[1], 48);
			printf("\n");*/
		
			/*printf("\nsortie xor avant le xor : " );
			afficherTab(sortieXOR[0], 48);*/
			sortieXOR[0][11] = XOR (cle[0], partieGauche[0][11]);
			sortieXOR[0][13] = XOR (cle[1], partieGauche[0][13]);
			sortieXOR[0][23] = XOR (cle[2], partieGauche[0][23]);
			sortieXOR[0][25] = XOR (cle[3], partieGauche[0][25]);
			sortieXOR[0][33] = XOR (cle[4], partieGauche[0][33]);
			/*printf("\nsortie xor apres le xor : " );
			afficherTab(sortieXOR[0], 48);*/

			/*printf("\nsortie xor' avant' le xor : " );
			afficherTab(sortieXOR[1], 48);*/
			sortieXOR[1][11] = XOR (cle[0], partieGauche[1][11]);
			sortieXOR[1][13] = XOR (cle[1], partieGauche[1][13]);
			sortieXOR[1][23] = XOR (cle[2], partieGauche[1][23]);
			sortieXOR[1][25] = XOR (cle[3], partieGauche[1][25]);
			sortieXOR[1][33] = XOR (cle[4], partieGauche[1][33]);
			/*printf("\nsortie xor' apres le xor : " );
			afficherTab(sortieXOR[1], 48);*/
		
			copieTab(partieGauche[0], sortieXOR[0], 48);
			copieTab(partieGauche[1], sortieXOR[1], 48);
		
			SBox(partieGauche[0]);
			SBox(partieGauche[1]);
		
			P_Permutation(partieGauche[0]);
			P_Permutation(partieGauche[1]);
			
			// On XOR avec la partieDroite
			copieTab(sortieXOR[0], partieGauche[0], 32);
			copieTab(sortieXOR[1], partieGauche[1], 32);
			/*printf("2 partie droite : ");
			afficherTab(partieDroite[0], 32);
			printf("\n");
			printf("2 partie droite' : ");
			afficherTab(partieDroite[1], 32);
			printf("\n");*/
			for (k = 0; k < 32; k++) {
				sortieXOR[0][k] = XOR (partieGauche[0][k], partieDroite[0][k]);
				sortieXOR[1][k] = XOR (partieGauche[1][k], partieDroite[1][k]);
			}
			copieTab(partieGauche[0], sortieXOR[0], 32);
			copieTab(partieGauche[1], sortieXOR[1], 32);
			
			// On réalise le XOR entre les deux messages
			for (k = 0; k < 32; k++) {
				sortieXOR[0][k] = XOR (partieGauche[0][k], partieGauche[1][k]);
			}
			/*printf("[%i] XOR Y Y' : ", j/2);
			afficherTab(sortieXOR[0], 32);
			printf("\n");*/

			/*printf("xor attendu : ");
			afficherTab(dR, 32);
			printf("\n");
			printf("xor attendu en decimal : %i", bin_to_dec(dR, 32));
			printf("\n");*/
			if (bin_to_dec(sortieXOR[0], 32) == bin_to_dec(dR, 32)) {
				printf("[%i] XOR Y Y' : ", j/2);
				afficherTab(sortieXOR[0], 32);
				printf("\n");
			}
		}
	}
	
	return 0;
}
