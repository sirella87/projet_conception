#include <stdio.h>

int puissance (int n, int exp) {
	int produit = 1;
	
	int i;
	for (i = 1; i <= exp; i++)
		produit *= n;
		
	return produit;
}

unsigned int bin_to_dec(int bin[], int nb_bits) {
	int dec = 0;

	int i;
	for (i = 0; i < nb_bits; i++)
		dec += bin[i] * puissance(2,nb_bits-i-1);
	
	return dec;
}

void dec_to_bin(unsigned int dec, int nb_bits, int bin[]) {
	int reste;

	int i;
	for (i = nb_bits-1; i >= 0; i--) {
		reste = dec%2;
		dec = dec/2;
		bin[i] = reste;
		//printf("bin[%d] = %d", i, bin[i]);
	}
	
	if (dec > 1)
		printf("Erreur : Dépassement de capacité\n");
}

int hex_to_dec(char hex) {
	if (hex >= '0' && hex <= '9')
		return hex-48; // 0 dans la table ASCII = 48
		
	else {
		if (hex == 'A' || hex == 'a')
			return 10;
		if (hex == 'B' || hex == 'b')
			return 11;
		if (hex == 'C' || hex == 'c')
			return 12;
		if (hex == 'D' || hex == 'd')
			return 13;
		if (hex == 'E' || hex == 'e')
			return 14;
		if (hex == 'F' || hex == 'f')
			return 15;
	}
}

int estASCII (char car) {
	if (car >= 0 && car <= 127)
		return 0;
	else
		return 1;
}

int verifChaine (char chaine[]) {
	/*
	 * Vérifie qu'une chaine ne contient que des caractères ASCII
	 * Renvoie -1 si erreur, sinon renvoie la taille de la chaine
	 */
	 
	int taille = 0;
	while(chaine[taille] != '\0') {
		if (estASCII(chaine[taille]) != 0)
			return -1;
		taille++;
	}
	
	return taille;
}

void binaireChaine (int chaine_binaire[], int taille_chaine_binaire)
{
	int i,j;
	int car = 0;
	int num_bit = 7;
	
	for (i = 0; i < taille_chaine_binaire; i++) {
		car += chaine_binaire[i] * puissance(2,num_bit);
		num_bit--;
		
		if (num_bit < 0) {
			printf("%c", car);
			num_bit = 7;
			car = 0;
		}
	}
}

void binaireHexa (int chaine_binaire[], int taille_chaine_binaire)
{
	int i,j;
	int car = 0;
	int num_bit = 3;
	
	for (i = 0; i < taille_chaine_binaire; i++) {
		car += chaine_binaire[i] * puissance(2,num_bit);
		num_bit--;
		
		if (num_bit < 0) {
			printf("%x", car);
			num_bit = 3;
			car = 0;
		}
	}
}

void chaineBinaire (char chaine_char[], int taille_chaine, int chaine_bin[], int taille_chaine_binaire)
{
	// Transformation caractères -> entiers
	int chaine_int[taille_chaine];
	int i;
	for (i = 0; i < taille_chaine; i++) {
		chaine_int[i] = (int) chaine_char[i];
	}
	
	// Transformation entiers -> binaire
	int tmp[8]; // pour stocker les bits de chaque caractères
	int n, reste, nb_bits;
	int num_bit = 0; // numéro de chaque bit dans la chaine_binaire
	
	/* Calcul du nombre de bits à rajouter pour avoir un bloc de 64 */
	int nb_bits_manquants = taille_chaine_binaire - taille_chaine*8;
	
	int j;
	for (i = 0; i < taille_chaine; i++) {
		n = chaine_int[i];
		
		// Rentre les premiers bits
		nb_bits = 0;
		for (j = 7; j >= 0; j--) {
			reste = n%2;
			tmp[j] = reste;
			n = n/2;
			nb_bits++;
		}
		
		// Ajoute les zeros manquants
		while (nb_bits < 8) {
			printf("0");
			tmp[8-nb_bits] = 0;
			nb_bits++;
		}
		
		// Ajoute chaque bit du caractère dans la chaine binaire
		for (j = 0; j < 8; j++) {
			chaine_bin[num_bit] = tmp[j];
			num_bit++;
		}
	}
	
	/* ajoute les bits manquants */
	if (nb_bits_manquants != 64) {
		for (i = 0; i < nb_bits_manquants; i++) {
			chaine_bin[num_bit] = 0;
			num_bit++;
		}
	}	
}

void hexaEnBinaire (char chaine_hexa[], int taille_chaine, int chaine_bin[], int taille_chaine_binaire)
{
	// Transformation hexa -> binaire
	int tmp[4];
	
	/* Calcul du nombre de bits à rajouter pour avoir un bloc de 64 */
	int nb_bits_manquants = taille_chaine_binaire - taille_chaine*4;
	
	int i, j;
	int num_bit = 0;
	for (i = 0; i < taille_chaine; i++) { // on transforme chaque caractere hexa en chaine binaire de taille 4
		dec_to_bin(hex_to_dec(chaine_hexa[i]), 4, tmp);
		for (j = 0; j < 4; j++) {
			chaine_bin[num_bit] = tmp[j];
			num_bit++;
		}
	}
	
	/* ajoute les bits manquants */
	if (nb_bits_manquants != 64) {
		for (i = 0; i < nb_bits_manquants; i++) {
			chaine_bin[num_bit] = 0;
			num_bit++;
		}
	}	
}
