#include <stdio.h>
#include <string.h>

#include "../inc/chaines.h"

void header (char script[]) {
	printf("Projet de L3 : Cryptanalyse linéaire et différentielle du DES\n");
	printf("==> %s\n\n", script);
}

void man (char script[]) {
	if (strcmp(script, "./crypt.e") == 0)
		printf("Usage : %s \"clé de chiffrement\" \"texte à crypter\"\n", script);
		
	else if (strcmp(script, "./decrypt.e") == 0)
		printf("Usage : %s \"clé de chiffrement\" \"texte à décrypter\"\n", script);
		
	else
		printf("Pas d'aide pour ce script\n");
}

int verifArguments(int argc, char *argv[], int texte_binaire[], int cle_binaire[64]) {
	int taille_texte, taille_cle;
	char *texte, *cle;

	if (argc != 3) { /* manque des arguments -> on affiche le man du script lancé*/
		printf("## Erreur : Il manque des arguments ##\n");
		man(argv[0]);
		return -1;
	}
		
	else { /* on verifie que les arguments sont corrects */
		/* Argument 1 : Clé */
		taille_cle = verifChaine(argv[1]);

		if (taille_cle == -1) {
			printf("## Erreur : La clé ne doit contenir que des caractères ASCII ##\n");
			return -1;
		}
		
		if (taille_cle > 8) {
			printf("## Erreur : La clé ne doit pas dépasser 8 caractères ##\n");
			return -1;
		}
		
		cle = argv[1];
		
		/* Argument 2 : Texte */
		taille_texte = verifChaine(argv[2]);
		if (taille_texte == -1) {
			printf("## Erreur : Le texte ne doit contenir que des caractères ASCII ##\n");
			return -1;
		}
		
		texte = argv[2];
	}
	
	// Affiche les paramètres d'entrée
	printf("Clé utilisée pour le chiffrement : %s\n", cle);
	// Transforme la clé en binaires
	chaineBinaire(cle, taille_cle, cle_binaire, 64);
	int taille_texte_binaire;
	int nb_bits_manquants;
	
	if (strcmp(argv[0], "./crypt.e") == 0) {
		printf("Texte à crypter : %s\n", texte);
	
		// Transforme la clé en binaires
		chaineBinaire(cle, taille_cle, cle_binaire, 64);
	
		// Transforme le texte en binaire et ajoute les bits manquants
		nb_bits_manquants = 0;
		if (64 - taille_texte*8 % 64 != 64)
			nb_bits_manquants = 64 - taille_texte*8 % 64;
		taille_texte_binaire = taille_texte*8 + nb_bits_manquants;
		chaineBinaire(texte, taille_texte, texte_binaire, taille_texte_binaire);
	}
	
	else if (strcmp(argv[0], "./decrypt.e") == 0) {
		printf("Texte à décrypter : %s\n", texte);
	
		// Transforme l'hexa en binaire et ajoute les bits manquants
		nb_bits_manquants = 0;
		if (64 - taille_texte*4 % 64 != 64)
			nb_bits_manquants = 64 - taille_texte*4 % 64;
		taille_texte_binaire = taille_texte*4 + nb_bits_manquants;
		hexaEnBinaire(texte, taille_texte, texte_binaire, taille_texte_binaire);
	}
	
	return taille_texte_binaire;
}
