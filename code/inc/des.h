#ifndef DES_H
#define DES_H

int XOR(int a, int b);
void reductionClef1(int K[]);
void rotation(int K[], int n);
void reductionClef2(int K[]);
void G(int K[],int clefTour[48][16], int nb_tours, int crypt);
void PermutInitiale(int w[]);
void PermutFinale(int w[]);
void P_Permutation(int w[]);
void EtendreDonnees(int R[]);
void SBoite(int entree[6], int sortie[4], int numSbox);
void expansion(int mat[32], int matEtendue[48]);
void inverseExpansion(int matEtendue[48], int mat[32]);
void concatene(int Tab1[], int Tab2[],int Tab3[],int taille);
void DiviseDonnees(int tab[],int L[],int R[],int taille);

void des (int crypt, int donnees[64], int resultat[64], int cle_binaire[64], int nb_tours);

#endif
