#ifndef CRYPTANALYSE_DIF_H
#define CRYPTANALYSE_DIF_H

int probaSbox (int vectTabDiff[8][64][16], int numSbox, int dMEtendu[48], int dM[32]);
void cryptanalyse (char *nomFichier);

#endif
