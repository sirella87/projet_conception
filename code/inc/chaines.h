#ifndef CHAINES_H
#define CHAINES_H

int tailleChaine (char chaine[]);
void binaireChaine (int chaine_binaire[], int taille_chaine_binaire);
void chaineBinaire (char chaine_char[], int taille_chaine, int chaine_bin[], int taille_chaine_binaire);
void binaireHexa (int chaine_binaire[], int taille_chaine_binaire);
void hexaEnBinaire (char chaine_hexa[], int taille_chaine, int chaine_bin[], int taille_chaine_binaire);
int verifChaine (char chaine[]);
unsigned int bin_to_dec(int bin[], int nb_bits);
void dec_to_bin(unsigned int dec, int nb_bits, int bin[]);

#endif
